# php-extended/php-http-client-simple-cache-psr16
A psr-18 compliant middleware client that handles requests through a psr-16 compliant simple-cache.

![coverage](https://gitlab.com/php-extended/php-http-client-simple-cache-psr16/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-http-client-simple-cache-psr16/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-http-client-simple-cache-psr16 ^8`


## Basic Usage

This library is to make a man in the middle for http requests and responses
and logs the events when requests passes. It may be used the following way :

```php

/* @var $client Psr\Http\Client\ClientInterface */    // psr-18
/* @var $cache Psr\SimpleCache\CacheInterface */      // psr-16
/* @var $requestFactory \Psr\Http\Message\RequestFactoryInterface */ // psr-17
/* @var $streamFactory  \Psr\Http\Message\StreamFactoryInterface */  // psr-17
/* @var $request Psr\Http\Message\RequestInterface */ // psr-7

$client = new SimpleCacheClient($client, $cache, $requestFactory, $streamFactory);
$response = $client->sendRequest($request);

/* @var $response Psr\Http\Message\ResponseInterface */

```

This library handles different headers :
- For the requests :
  - Cache-Control
  - Pragma
  - Date
  - If-Match
  - If-Modified-Since
  - If-None-Match
  - If-Range
  - If-Unmodified-Since
- For the responses :
  - Cache-Control
  - Pragma
  - Date
  - Expires
  - ETag
  - Last-Modified
  - Age


## License

MIT (See [license file](LICENSE)).
