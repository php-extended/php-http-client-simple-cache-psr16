<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-simple-cache-psr16 library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use DateTimeInterface;
use Stringable;

/**
 * SimpleCacheData class file.
 * 
 * This class is just a container to handle data to put and retrieve to cache.
 * 
 * @author Anastaszor
 */
class SimpleCacheData implements Stringable
{
	
	/**
	 * The date when this cache data was registered.
	 * 
	 * @var ?DateTimeInterface
	 */
	public ?DateTimeInterface $since = null;
	
	/**
	 * The protocol version.
	 * 
	 * @var ?string
	 */
	public ?string $protocolVersion = null;
	
	/**
	 * The request method.
	 * 
	 * @var ?string
	 */
	public ?string $requestMethod = null;
	
	/**
	 * The url of the request.
	 * 
	 * @var ?string
	 */
	public ?string $requestUrl = null;
	
	/**
	 * The headers of the request.
	 * 
	 * @var array<string, array<int, string>>
	 */
	public array $requestHeaders = [];
	
	/**
	 * The response code.
	 * 
	 * @var ?integer
	 */
	public ?int $responseCode = null;
	
	/**
	 * The response reason phrase.
	 * 
	 * @var ?string
	 */
	public ?string $responsePhrase = null;
	
	/**
	 * The headers of the response.
	 * 
	 * @var array<string, array<int, string>>
	 */
	public array $responseHeaders = [];
	
	/**
	 * The full response body.
	 * 
	 * @var ?string
	 */
	public ?string $responseBody = null;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
}
