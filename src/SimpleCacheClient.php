<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-simple-cache-psr16 library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use DateInterval;
use DateTimeImmutable;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\SimpleCache\CacheInterface;
use Psr\SimpleCache\InvalidArgumentException;
use Stringable;

/**
 * SimpleCacheClient class file.
 * 
 * This class is a client that checks for cache entries when handling requests.
 * 
 * @author Anastaszor
 */
class SimpleCacheClient implements ClientInterface, Stringable
{
	
	/**
	 * The inner client.
	 * 
	 * @var ClientInterface
	 */
	protected ClientInterface $_client;
	
	/**
	 * The inner cache.
	 * 
	 * @var CacheInterface
	 */
	protected CacheInterface $_cache;
	
	/**
	 * The response factory.
	 * 
	 * @var ResponseFactoryInterface
	 */
	protected ResponseFactoryInterface $_responseFactory;
	
	/**
	 * The stream factory.
	 * 
	 * @var StreamFactoryInterface
	 */
	protected StreamFactoryInterface $_streamFactory;
	
	/**
	 * The simple cache configuration.
	 * 
	 * @var SimpleCacheConfiguration
	 */
	protected SimpleCacheConfiguration $_configuration;
	
	/**
	 * Builds a new SimpleCacheClient with the given inner client, cache, 
	 * response factories and configuration.
	 * 
	 * @param ClientInterface $client
	 * @param CacheInterface $cache
	 * @param ResponseFactoryInterface $responseFactory
	 * @param StreamFactoryInterface $streamFactory
	 * @param ?SimpleCacheConfiguration $config
	 */
	public function __construct(ClientInterface $client, CacheInterface $cache, ResponseFactoryInterface $responseFactory, StreamFactoryInterface $streamFactory, ?SimpleCacheConfiguration $config = null)
	{
		$this->_client = $client;
		$this->_cache = $cache;
		$this->_responseFactory = $responseFactory;
		$this->_streamFactory = $streamFactory;
		if(null === $config)
		{
			$config = new SimpleCacheConfiguration();
		}
		$this->_configuration = $config;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/*
	 * This method supports the following feature table :
	 *
	 * REQUEST :
	 *
	 * Cache-Control: max-age                NO
	 * Cache-Control: max-stale              NO
	 * Cache-Control: min-fresh              NO
	 * Cache-Control: no-cache               YES
	 * Cache-Control: no-store               YES
	 * Cache-Control: no-transform           NO
	 * Cache-Control: only-if-cached         NO
	 * Cache-Control: stale-while-revalidate NO
	 * Cache-Control: stale-if-error         NO
	 * Clear-Site-Data: cache *              NO
	 * Pragma: no-cache                      YES
	 * Date: <date>                          NO
	 * Vary: <header>                        NO
	 *
	 *
	 * Use If-Match                          NO
	 * Use If-Modified-Since                 NO
	 * Use If-None-Match                     NO
	 * Use If-Range                          NO
	 * Use If-Unmodified-Since               NO
	 *
	 * RESPONSE :
	 *
	 * Cache-Control: must-revalidate        NO
	 * Cache-Control: no-cache               NO
	 * Cache-Control: no-store               NO
	 * Cache-Control: no-transform           NO
	 * Cache-Control: public                 NO
	 * Cache-Control: private                NO
	 * Cache-Control: proxy-revalidate       NO
	 * Cache-Control: max-age                NO
	 * Cache-Control: s-maxage               NO
	 * Cache-Control: immutable              NO
	 * Date: <date>                          NO
	 * Expires: <date>                       NO
	 * Pragma: no-cache                      NO
	 * ETag: <tag>                           NO
	 * Last-Modified : <date>                NO
	 *
	 * Adding Age Header                     YES
	 */
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Client\ClientInterface::sendRequest()
	 */
	public function sendRequest(RequestInterface $request) : ResponseInterface
	{
		// STEP 0 : define whether the request is eligible for caching
		// if the method is not get or options or head, do not cache it and
		// forwards the call to the inner client
		if(!\in_array(\mb_strtoupper($request->getMethod()), ['GET', 'OPTIONS', 'HEAD'], true))
		{
			return $this->_client->sendRequest($request);
		}
		
		// primary : if the cache is configured to enforce request caching,
		// then ignore the cache directives from requests.
		$doCacheLookup = !$this->_configuration->shouldEnforceRequestDefaultCache()
			|| $this->doCacheLookupCacheControl($request)
			|| $this->doCacheLookupPragma($request);
		if(!$doCacheLookup)
		{
			return $this->_client->sendRequest($request);
		}
		
		// STEP 1 : forges the hash of the request by taking the query string
		// and the relevant headers of the request. No unmixing by ordering
		// query parameters or headers is done because of the overhead.
		// This will lead to different requests with the same parameters but
		// where the order is different to be registered as separated requests.
		$hash = $this->getRequestHash($request);
		
		// STEP 2 : lookup the cache for such request, if the request exists
		// and is not expired, then returns the accordingly forged response
		try
		{
			// does not recognize \Psr\Cache\InvalidArgumentException
			// because it does not inherit \Throwable
			/** @psalm-suppress MissingThrowsDocblock */
			$item = $this->_cache->get($hash);
			if($item instanceof SimpleCacheData)
			{
				$cachedResponse = $this->getCachedResponse($item);
				if(null !== $cachedResponse)
				{
					return $cachedResponse;
				}
			}
		}
		catch(InvalidArgumentException $exc)
		{
			// just ignore and proceed
		}
		
		// STEP 3 : if the cache does not exists, or is expired, forwards the
		// request to the inner client
		$response = $this->_client->sendRequest($request);
		
		// STEP 4 : cache the response, and returns a response
		// do not cache 400 or 500 error codes
		return $this->cacheResponse($request, $response, $hash);
	}
	
	/**
	 * Gets the headers that are expected to produce a different page if
	 * their values are different from the same request.
	 * 
	 * @return array<integer, string>
	 */
	protected function getCacheRelevantHeaders() : array
	{
		return [
			'Cookie',
			'If-Match',
			'If-Modified-Since',
			'If-None-Match',
			'If-Unmodified-Since',
		];
	}
	
	/**
	 * Gets whether this response is eligible to cache.
	 * 
	 * @param ResponseInterface $response
	 * @return boolean
	 */
	protected function isResponseEligibleToCache(ResponseInterface $response) : bool
	{
		return \in_array($response->getStatusCode(), [200, 204, 206, 301, 308, 404, 405, 410], true);
	}
	
	/**
	 * Calculates the cache duration based on the request, the response and 
	 * the configuration of this cache client.
	 * 
	 * @param RequestInterface $request
	 * @param ResponseInterface $response
	 * @return DateInterval
	 * @SuppressWarnings("PHPMD.UnusedFormalParameter")
	 */
	protected function resolveCachingDuration(RequestInterface $request, ResponseInterface $response) : DateInterval
	{
		return $this->_configuration->getDefaultDuration();
	}
	
	/**
	 * Gets the age value, defaults to zero when the header is not present.
	 * 
	 * @param ResponseInterface $response
	 * @return int
	 */
	protected function getAgeHeaderValue(ResponseInterface $response) : int
	{
		$age = 0;
		$ageHeader = $response->getHeader('Age');
		
		foreach($ageHeader as $header)
		{
			if(\is_numeric($header))
			{
				$age = \max($age, (int) $header);
			}
		}
		
		return $age;
	}
	
	/**
	 * Gets the number of seconds represented by the given interval.
	 * 
	 * @param DateInterval $interval
	 * @return integer
	 */
	protected function toSeconds(DateInterval $interval) : int
	{
		$secs = 0;
		$days = (int) $interval->format('%a');
		$secs += $days * 24 * 60 * 60;
		$secs += ((int) $interval->h) * 60 * 60;
		$secs += ((int) $interval->i) * 60;
		$secs += ((int) $interval->s);
		
		return $secs;
	}
	
	/**
	 * Gets a string representing a suitable key for this request.
	 * 
	 * @param RequestInterface $request
	 * @return string
	 */
	protected function getRequestHash(RequestInterface $request) : string
	{
		$headerParts = [];
		
		foreach($this->getCacheRelevantHeaders() as $header)
		{
			$headers = $request->getHeader($header);
			if(!empty($headers))
			{
				$headerParts[] = $header.' : '.\implode(', ', $headers);
			}
		}
		$fullHeaders = \implode(' | ', $headerParts);
		$requestUri = $request->getUri()->__toString();
		
		return \str_replace(
			['{', '}', '(', ')', '/', '\\', '@', ':'], // forbidden characters psr-6
			'-',
			$request->getMethod().' '.$requestUri.' ['.$fullHeaders.']',
		);
	}
	
	/**
	 * Caches the given response and returns the response.
	 * 
	 * @param RequestInterface $request
	 * @param ResponseInterface $response
	 * @param string $hash
	 * @return ResponseInterface
	 */
	protected function cacheResponse(RequestInterface $request, ResponseInterface $response, string $hash) : ResponseInterface
	{
		if($this->isResponseEligibleToCache($response))
		{
			$cacheData = new SimpleCacheData();
			$cacheData->since = new DateTimeImmutable();
			$cacheData->protocolVersion = $response->getProtocolVersion();
			$cacheData->requestMethod = $request->getMethod();
			$cacheData->requestUrl = $request->getUri()->__toString();
			/** @psalm-suppress MixedPropertyTypeCoercion */
			$cacheData->requestHeaders = $request->getHeaders();
			$cacheData->responseCode = $response->getStatusCode();
			$cacheData->responsePhrase = $response->getReasonPhrase();
			/** @psalm-suppress MixedPropertyTypeCoercion */
			$cacheData->responseHeaders = $response->getHeaders();
			$cacheData->responseBody = $response->getBody()->__toString();
			
			try
			{
				$this->_cache->set($hash, $cacheData, $this->resolveCachingDuration($request, $response));
			}
			catch(InvalidArgumentException $exc)
			{
				// nothing to do
			}
		}
		
		return $response;
	}
	
	/**
	 * Whether we should do the caching according to the Cache-Control header.
	 * 
	 * @param RequestInterface $request
	 * @return boolean
	 */
	protected function doCacheLookupCacheControl(RequestInterface $request) : bool
	{
		$ccHeader = $request->getHeader('Cache-Control');
		if(!empty($ccHeader))
		{
			$ccHeader = \array_map('strtolower', $ccHeader); // normalize
			$ccHeaders = []; // split by comma and renormalize
			
			foreach($ccHeader as $ccHeaderx)
			{
				$ccHeaders = \array_merge($ccHeaders, \array_map('trim', \explode(',', $ccHeaderx)));
			}
			
			// if the Cache-Control header is set to
			// - no-store : ignore it
			if(\in_array('no-store', $ccHeaders, true))
			{
				return false;
			}
			
			// - no-cache : then skip cache lookup
			if(\in_array('no-cache', $ccHeaders, true))
			{
				return false;
			}
			
			// - public : nothing happens
			// - private : check for the configuration to see if we are a private
			//             cache, if we are, proceed, if not ignore the request
			if(\in_array('private', $ccHeaders, true) && !$this->_configuration->isPrivateCache())
			{
				return false;
			}
		}
		
		return false;
	}
	
	/**
	 * Whether we should do the caching according to the Pragma header.
	 * 
	 * @param RequestInterface $request
	 * @return boolean
	 */
	protected function doCacheLookupPragma(RequestInterface $request) : bool
	{
		// if Cache-Control header exists, dont care about pragma
		$pHeader = $request->getHeader('Pragma');
		if(!empty($pHeader))
		{
			$pHeader = \array_map('strtolower', $pHeader); // normalize
			
			// if the Pragma header is set to no-cache
			// then skip cache lookup
			return !(\in_array('no-cache', $pHeader, true));
		}
		
		return false;
	}
	
	/**
	 * Gets an updated response from the cache, if any is available and valid.
	 * 
	 * @param SimpleCacheData $cacheData
	 * @return ?ResponseInterface
	 */
	protected function getCachedResponse(SimpleCacheData $cacheData) : ?ResponseInterface
	{
		$response = $this->_responseFactory->createResponse((int) $cacheData->responseCode, (string) $cacheData->responsePhrase);
		$response = $response->withProtocolVersion((string) $cacheData->protocolVersion);
		
		foreach($cacheData->responseHeaders as $k => $values)
		{
			try
			{
				$response = $response->withAddedHeader($k, $values);
			}
			catch(\InvalidArgumentException $exc)
			{
				// nothing to do
			}
		}
		
		if(null === $cacheData->responseBody || '' === $cacheData->responseBody)
		{
			try
			{
				$response = $response->withBody($this->_streamFactory->createStream((string) $cacheData->responseBody));
			}
			catch(\InvalidArgumentException $exc)
			{
				// nothing to do
			}
		}
		
		$previousAge = $this->getAgeHeaderValue($response);
		// calculate the Age value
		$now = new DateTimeImmutable();
		if(null !== $cacheData->since)
		{
			$interval = $now->diff($cacheData->since);
			$ageSec = $this->toSeconds($interval);
			
			try
			{
				return $response->withHeader('Age', (string) ($previousAge + $ageSec));
			}
			catch(\InvalidArgumentException $exc)
			{
				// just ignore and proceed
			}
		}
		
		return null;
	}
	
}
