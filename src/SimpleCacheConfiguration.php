<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-simple-cache-psr16 library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use DateInterval;
use Stringable;

/**
 * SimpleCacheConfiguration class file.
 * 
 * This class represents the configuration for the simple cache.
 * 
 * @author Anastaszor
 */
class SimpleCacheConfiguration implements Stringable
{
	
	/**
	 * Whether to enforce the request default cache duration, meaning to ignore
	 * the cache-control directives from the requests.
	 * 
	 * @var boolean
	 */
	protected bool $_enforceRequestDefaultCache = false;
	
	/**
	 * Whether to enforce the response default cache duration, meaning to ignore
	 * the cache-control directives from the responses. 
	 * 
	 * @var boolean
	 */
	protected bool $_enforceResponseDefaultCache = false;
	
	/**
	 * The default cache duration.
	 * 
	 * @var ?DateInterval
	 */
	protected ?DateInterval $_defaultDuration = null;
	
	/**
	 * Whether this cache is a private cache.
	 * 
	 * @var boolean
	 */
	protected bool $_isPrivateCache = false;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Enables the enforcement for request default cache duration.
	 */
	public function enableEnforceRequestDefaultCache() : void
	{
		$this->_enforceRequestDefaultCache = true;
	}
	
	/**
	 * Disables the enforcement for request default cache duration.
	 */
	public function disableEnforceRequestDefaultCache() : void
	{
		$this->_enforceRequestDefaultCache = false;
	}
	
	/**
	 * Gets whether to enforce the request default cache value.
	 * 
	 * @return boolean
	 */
	public function shouldEnforceRequestDefaultCache() : bool
	{
		return $this->_enforceRequestDefaultCache;
	}
	
	/**
	 * Enables the enforcement for response default cache duration.
	 */
	public function enableEnforceResponseDefaultCache() : void
	{
		$this->_enforceResponseDefaultCache = true;
	}
	
	/**
	 * Disables the enforcement for response default cache duration.
	 */
	public function disableEnforceResponseDefaultCache() : void
	{
		$this->_enforceResponseDefaultCache = false;
	}
	
	/**
	 * Gets whether to enforce the response default cache value.
	 * 
	 * @return boolean
	 */
	public function shouldEnforceResponseDefaultCache() : bool
	{
		return $this->_enforceResponseDefaultCache;
	}
	
	/**
	 * Sets the default cache duration as time interval.
	 * 
	 * @param DateInterval $interval
	 */
	public function setDefaultDuration(DateInterval $interval) : void
	{
		$this->_defaultDuration = $interval;
	}
	
	/**
	 * Gets the default cache duration.
	 * 
	 * @return DateInterval
	 * @psalm-suppress InvalidFalsableReturnType
	 */
	public function getDefaultDuration() : DateInterval
	{
		if(null === $this->_defaultDuration)
		{
			// default of default value is 10 hours, should change twice a day
			/** @psalm-suppress PossiblyFalsePropertyAssignmentValue */
			$this->_defaultDuration = DateInterval::createFromDateString('+10 hours');
		}
		
		/** @psalm-suppress FalsableReturnStatement */
		return $this->_defaultDuration;
	}
	
	/**
	 * Enables the private status of this cache.
	 */
	public function enableIsPrivateCache() : void
	{
		$this->_isPrivateCache = true;
	}
	
	/**
	 * Disables the private status of this cache.
	 */
	public function disableIsPrivateCache() : void
	{
		$this->_isPrivateCache = false;
	}
	
	/**
	 * Gets whether this cache is a private cache.
	 * 
	 * @return boolean
	 */
	public function isPrivateCache() : bool
	{
		return $this->_isPrivateCache;
	}
	
}
